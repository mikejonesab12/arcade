
(function() {
    var my = {};


    my.onInit = function() {

    };

    my.onLoad = function() {
        InitTouchKeyboard();
        nfcPoll();
    };

    my.events = {
        "click #exit": exit,
        "click #loginPoints": authenticate
        
    };

    my.onUnload = function() {
           stopPoll();
    };

    function exit() {
        $("#mask").hide();
    }


    function authenticate() {
        var username = document.getElementById("username").value;
        var password = document.getElementById("password").value;

        var obj = iSnap.System.DocumentStore.Get("Arcade", "Flappybird", "accounts");
        for (var i = 0; i < obj.accounts.length; i++) {
            if (obj.accounts[i].username === username && obj.accounts[i].password === password) {
                app.state.points = obj.accounts[i].points;
                app.state.user = obj.accounts[i].username;
                document.getElementById("futurePoints").innerHTML += '<button id ="pointTotal" type="button" class="btn btn-default btn-lg">'+
                                                                    ''+ app.state.points +''+
                                                                    '</button> ';
                $("#mask").hide();
                return;
            }
            else {
                document.getElementById("loginError").style.display = 'block';
                $("#loginError").effect("shake");
            }
        }
    }
    
    function nfcPoll(){
    app.state.nfcCheck = setInterval(function (){
            var x = iSnap.System.NFC.GetNFCTag();
            if (x !== 0){
                stopPoll();
                app.state.nfc = x;
                var obj = iSnap.System.DocumentStore.Get("Arcade", "Flappybird", "accounts");
            
                for (var i = 0; i < obj.accounts.length; i++){
                    
                    if (obj.accounts[i].nfc == app.state.nfc ){
                        app.state.points = obj.accounts[i].points;
                        app.state.user = obj.accounts[i].username;
                        document.getElementById("futurePoints").innerHTML += '<button id ="pointTotal" type="button" class="btn btn-default btn-lg">'+
                                                                    ''+ app.state.points +''+
                                                                    '</button> ';
                        $("#mask").hide();
                        return;
                    }
                } 
            }
        }, 500);
    }
    
    function stopPoll(){
        clearInterval(app.state.nfcCheck);
    }

    return my;
}());


