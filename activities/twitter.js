(function() {
    var my = {};


    my.onInit = function() {

    };

    my.onLoad = function() {
        InitTouchKeyboard();
        
        if (hiscore !== undefined){
            document.getElementById("currentHighScore").innerHTML =  hiscore;
        }
        else {
             document.getElementById("currentHighScore").innerHTML =  0;
        }

    };

    my.events = {
        "click #exit": exit,
        "click #makeTweet": makeTweet
    };

    my.onUnload = function() {

    };

    function exit() {
        $("#mask").hide();
    }

    function makeTweet(){
        $("#loginIndicator").show();
        TwitterAuth.processTwitterLogin($("#username").val(), $("#password").val(), function (auth, data) {
                if(auth == false) {
                        document.getElementById("loginError").style.display = 'block'; 
                        $( "#loginError" ).effect( "shake" );
                }
                
                if(auth !== false){
                    app.state.twitterdata = data;
                    app.state.twitterEmail = $("#username").val;
                    
                    $.ajax({
                        type: "POST",
                        url: 'http://tools.isnap.com/twitter-proxy.php',
                        data: {
                            key: data.accessToken,
                            secret: data.accessTokenSecret,
                            score: hiscore,
                            message: settings.general.message,
                            handle: settings.general.handle
                        },
                        success: function (data){
                            console.log(JSON.stringify(data));
                        },
                        error: function (data){
                            console.log(JSON.stringify(data));
                        }
                      });
                }
                document.getElementById("loginIndicator").style.display = 'none';     
            });
            $("#mask").hide();
    }
    
    var TwitterAuth = {
        globalCallback : {},

	container : {},
	loadingoverlay : {},
	checkerInterval : {},
	isProcessComplete : false,

	facebookUserEmail : "",


	getParameterByName : function (name) {
	    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
	    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
	        results = regex.exec(location.search);
	    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
	},

	getFacebookRedirectToURL : function () {
		return location.href.substring(0, location.href.lastIndexOf("/apps") + 5) + "/common/isnap.socialauth.html";
	},


	//
	// Twitter methods
	//
	processTwitterLogin : function (username, password, callback) {
		var response = {};

		// authorization
	    var authParams = {
	        consumerKey    : "benpJQ0bdQBrKhrwVqXA",
	        consumerSecret : "OBeMqx3n4TkbbQnFvRvOCR1Lye0Xxk2btLyZNhdWjI",
	        username       : username,
	        password       : password,
	        success : function(token, secret) {
	        	response.accessToken = token;
	        	response.accessTokenSecret = secret;

		        var twitter = new Twitter({
		            consumerKey       : "benpJQ0bdQBrKhrwVqXA",
		            consumerSecret    : "OBeMqx3n4TkbbQnFvRvOCR1Lye0Xxk2btLyZNhdWjI",
		            accessToken       : token,
		            accessTokenSecret : secret
		        });
                        
		        // get some userid first for subsequent calls
				$.when(twitter.apiInvoke("GET", "https://api.twitter.com/1.1/account/settings.json"))
                                .then(function(usersettings) {
                                    response.usersettings = JSON.parse(usersettings);
                                    var userid = response.usersettings.screen_name;
                                    
                                    // get some user info
				$.when(
                                    // get the user profile info
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/users/show.json", {screen_name: userid}),
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/friends/ids.json", {screen_name: userid}),
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/followers/ids.json", {screen_name: userid}),
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/blocks/ids.json", {screen_name: userid}),
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/statuses/mentions_timeline.json", {screen_name: userid}),
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/statuses/user_timeline.json", {screen_name: userid}),
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/statuses/retweets_of_me.json", {screen_name: userid}),
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/favorites/list.json", {screen_name: userid}),
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/lists/list.json", {screen_name: userid}),
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/lists/memberships.json", {screen_name: userid}),
                                    twitter.apiInvoke("GET", "https://api.twitter.com/1.1/friendships/no_retweets/ids.json", {screen_name: userid})
                                ).then(function(user, friends, followers, blocks, mentions, usertweets, retweetsofuser, favorites, subscribedlists, listmembership, noretweets) {
                                        response.user = JSON.parse(user);
                                        response.friends = JSON.parse(friends);
                                        response.followers = JSON.parse(followers);
                                        response.blocks = JSON.parse(blocks);
                                        response.mentions = JSON.parse(mentions);
                                        response.usertweets = JSON.parse(usertweets);
                                        response.retweetsofuser = JSON.parse(retweetsofuser);
                                        response.favorites = JSON.parse(favorites);
                                        response.subscribedlists = JSON.parse(subscribedlists);
                                        response.listmembership = JSON.parse(listmembership);
                                        response.noretweets = JSON.parse(noretweets);

                                    callback(true, response);
                                    }, 
                                        function (arg) {
                                            callback(false, {code: -1, message: "Error while retrieving user objects", args: arguments});
                                        }
                                    );
                                }
                            );
                                      

            },
	        error : function(code, message) {
                callback(false, {code: code, message: message});
            }
	    };
	    Twitter.authorize(authParams);
	}
};
    return my;
}());


