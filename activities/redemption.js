(function() {
    var my = {};
    

    my.onInit = function() {

    };

    my.onLoad = function() {
        if (app.state.points){
            document.getElementById("futurePoints").innerHTML += '<button type="button" class="btn btn-default btn-lg">'+
                                                                    ''+ app.state.points +''+
                                                                    '</button> ';
        }
       
    };

    my.events = {
        "click #home": goHome,
        "click #getPoints": login,
        "click #teddy": redeemPoints
    };

    my.onUnload = function() {
        app.state.points = undefined;
        app.state.points = undefined;
        
    };
    
    function login (){
        $("#mask").show();
        app.load("redemptionlogin", "#modal");
    }
    
    function goHome (){
        app.load("menu", "#appdoc");
    }
    
    function redeemPoints (){
        app.state.points = Number(app.state.points) - Number(3);
        var obj = iSnap.System.DocumentStore.Get("Arcade", "Flappybird", "accounts");
        for (var i = 0; i < obj.accounts.length; i++) {
            
            if (obj.accounts[i].username === app.state.user) {
                
                obj.accounts[i].points = app.state.points; 
                iSnap.System.DocumentStore.Put("Arcade", "Flappybird", "accounts" , obj); 
                break;
            }
        }
        document.getElementById('pointTotal').innerHTML = app.state.points;
    }

    return my;
}());


