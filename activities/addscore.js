(function() {
    var my = {};


    my.onInit = function() {

    };

    my.onLoad = function() {
        InitTouchKeyboard();
        nfcPoll();
        
        if (hiscore !== undefined){
            document.getElementById("currentHighScore").innerHTML = hiscore;
        }
        else {
             document.getElementById("currentHighScore").innerHTML =  0;
        }

    };

    my.events = {
        "click #exit": exit,
        "click #saveScore": authenticate
    };

    my.onUnload = function() {
        stopPoll();
    };

    function exit() {
        $("#mask").hide();
    }
    
    
    function authenticate (){
        var username = document.getElementById("username").value;
        var password =  document.getElementById("password").value;
        
        var obj = iSnap.System.DocumentStore.Get("Arcade", "Flappybird", "accounts");
        for (var i = 0; i < obj.accounts.length; i++){
                if (obj.accounts[i].username === username && obj.accounts[i].password === password){
                    addScores();
                    return;
                }
                else{
                    document.getElementById("loginError").style.display = 'block';
                    $("#loginError").effect("shake");
                }
            }
    }
    
    function addScores () {
        var record = {};
        record["name"] =  document.getElementById("username").value;
        record["score"] =  hiscore;
        record["date"] =  getDate();
        
        var user = iSnap.System.DocumentStore.Get("Arcade", "Flappybird", "accounts");
      
        
        
        for (var i = 0; i < user.accounts.length; i++){
            if (user.accounts[i].username === record["name"] ){
                if (user.accounts[i].points === undefined){
                    user.accounts[i].points = score;
                }
                else{
                    user.accounts[i].points = Number(score) + Number(user.accounts[i].points);
                }
            }
        }
        
        iSnap.System.DocumentStore.Put("Arcade", "Flappybird", "accounts" , user);
        
        var obj = iSnap.System.DocumentStore.Get("Arcade", "Flappybird", "scores");
        
        if (obj.scores !== undefined){
            obj.scores.push(record);
            iSnap.System.DocumentStore.Put("Arcade", "Flappybird", "scores" , obj);
        }
        else{
            obj.scores = [];
            obj.scores.push(record);
            iSnap.System.DocumentStore.Put("Arcade", "Flappybird", "scores" , obj); 
        }
        
        $("#mask").hide();
    }
    
    function getDate(){
        var today = new Date();
        var dd = today.getDate();
        var mm = today.getMonth() + 1; //January is 0!
        var yyyy = today.getFullYear();

        if (dd < 10) {
            dd = '0' + dd
        }

        if (mm < 10) {
            mm = '0' + mm
        }

        today = mm + '/' + dd + '/' + yyyy;
        return today;
    }
    
    function nfcPoll(){
        var record = {};
        record["name"] = "";
        record["score"] = hiscore;
        record["date"] =  getDate();
        
        app.state.nfcCheck = setInterval(function (){
            var x = iSnap.System.NFC.GetNFCTag();
            if (x !== 0){
                console.log(hiscore);
                stopPoll();
                app.state.nfc = x;
                var user = iSnap.System.DocumentStore.Get("Arcade", "Flappybird", "accounts");
            
                for (var i = 0; i < user.accounts.length; i++){
                    
                    if (user.accounts[i].nfc == app.state.nfc ){
                        record["name"] = user.accounts[i].username; 
                        if (user.accounts[i].points === undefined){
                            user.accounts[i].points = score;
                        }
                        else{
                            user.accounts[i].points = Number(score) + Number(user.accounts[i].points);
                        }
                        
                         iSnap.System.DocumentStore.Put("Arcade", "Flappybird", "accounts" , user);

                        var obj = iSnap.System.DocumentStore.Get("Arcade", "Flappybird", "scores");

                        if (obj.scores !== undefined){
                            obj.scores.push(record);
                            iSnap.System.DocumentStore.Put("Arcade", "Flappybird", "scores" , obj);
                        }
                        else{
                            obj.scores = [];
                            obj.scores.push(record);
                            iSnap.System.DocumentStore.Put("Arcade", "Flappybird", "scores" , obj); 
                        }

                        $("#mask").hide();
                    }
                } 
            }
        }, 500);
    }
    
    function stopPoll(){
        clearInterval(app.state.nfcCheck);
    }

    return my;
}());