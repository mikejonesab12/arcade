(function() {
    var my = {};
    app.state.nfcCheck; 

    my.onInit = function() {

    };

    my.onLoad = function() {
        InitTouchKeyboard();
        nfcPoll();
    };


    my.events = {
        "click #createAccount": createAccount,
         "click #exit": exit
    };

    my.onUnload = function() {
        stopPoll();
    };

    function createAccount (){
        var record = {};
        record["username"] =  document.getElementById('username').value;
        record["password"] = document.getElementById('password').value;
        record["nfc"] = app.state.nfc;
        
        var obj = iSnap.System.DocumentStore.Get("Arcade", "Flappybird", "accounts");
        
        if (obj.accounts !== undefined){
            for (var i = 0; i < obj.accounts.length; i++){
                if (obj.accounts[i].username === record["username"]){
                    document.getElementById("createError").style.display = 'block';
                    $("#createError").effect("shake");
                    return;
                }
            }
            obj.accounts.push(record);
            iSnap.System.DocumentStore.Put("Arcade", "Flappybird", "accounts" , obj);
        }
        else{
            obj.accounts = [];
            obj.accounts.push(record);
            iSnap.System.DocumentStore.Put("Arcade", "Flappybird", "accounts" , obj); 
        }
        stopPoll();
        $("#mask").hide();
    }
    
    function exit() {    
        $("#mask").hide();
    }
    
                  
    function nfcPoll(){
        app.state.nfcCheck = setInterval(function (){
            var i = iSnap.System.NFC.GetNFCTag();
            if (i !== 0){
                app.state.nfc =  i;
                document.getElementById("nfcRes").innerHTML = 'NFC ID Found: '+ i;
            }
        }, 500);
    }
            
    function stopPoll(){
        clearInterval(app.state.nfcCheck);
    }
    

    return my;
}());


