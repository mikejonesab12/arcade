(function() {
    var my = {};
    
    my.onInit = function() {

    };

    my.onLoad = function() {
        my.game = Game();
    };
    

    my.events = {
        "click #back": back,
        "click #leaderboard" : showScores,
        "click #savescore" : addScore,
        "click #tweetScore" : tweetScore
    };

    my.onUnload = function() {
        music.stop();
    };
    
    function back(){
        app.load("menu", "#appdoc");
        console.log("click");
    }
    
    function showScores(){
        $("#mask").show();
        app.load("scores", "#modal");
    }
    
    function addScore(){
       $("#mask").show();
        app.load("addscore", "#modal");
    }
    
    function tweetScore(){
       $("#mask").show();
        app.load("twitter", "#modal");
    }

    return my;
}());


