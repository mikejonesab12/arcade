var app = (function() {
	var my = {};
	my.documentlist = [];
	my.currentdocument = [];
	my.state = {};//variable dump for variables that need to persist
        my.media = {};
        
        my.appEntry =  function (init, callback){
            //app entry waits for document html to load as well as all ajax calls
            $(document).ready(function() {
                $(document).ajaxStop(function() {
                    $(this).unbind("ajaxStop"); //prevent running again when other calls finish
                    callback();
                });
            });
            
            my.init(init);
        };

	my.init = function (docs) {

		// preload each view and controller into our documentlist object
		for(i=0;i<docs.length; i++) {
			my.documentlist[i]={};

			my.documentlist[i].name = docs[i];

			// load the view into memory
			my.documentlist[i].view = $.ajax({
			       async: false,
			       url: "html/" + docs[i] + ".html",
			    }).responseText;

			// note that main javascript is eval'd and will return a controller object
			my.documentlist[i].controller = eval($.ajax({
			       async: false,
			       url: "activities/" + docs[i] + ".js",
			    }).responseText);

			// invoke the onInit event of the controller
			my.documentlist[i].controller.onInit();
                                          
		}
	};

	my.load = function (doc, target) {
            
                if (target == undefined){
                    target = "#document";
                }
               
		// if we had a current controller loaded then unload it
		if(my.currentdocument[target] != undefined) {
			my.currentdocument[target].controller.onUnload();
		}

		// find the document referenced
		var targetdoc = undefined;
		for(i=0;i<this.documentlist.length; i++) {
			if(this.documentlist[i].name == doc) {
				targetdoc = this.documentlist[i];
				break;
			}
		}

		if(targetdoc == undefined) {
			console.log("Unable to find document named: " + doc);
			return;
		}

		
		// store the current document for future use
		my.currentdocument[target] = targetdoc;
                
                // load the html               
                $(target).html(my.currentdocument[target].view);

		// invoke the load event of the controller
		my.currentdocument[target].controller.onLoad();
                
                //load controllers events
                attachEvents(my.currentdocument[target]);               
	};

        function attachEvents (targetdoc){
            for (var name in targetdoc.controller.events){
                var res = name.split(" ");
                (function (res, callback) {
                    $(res[1]).on(res[0], function() {
                       callback($(this));       
                    });       
                })(res, targetdoc.controller.events[name]);
            };
        }
        
        return my;

}());
